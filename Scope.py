import os,sys

# --------------------------------------------------------------------------------
# Scope IDE
#
# developed by lucidlylogicole
#
# Scope is licensed under the GNU General Public License (GPL 3)
# --------------------------------------------------------------------------------

def errorHandler(*exc_info):
    import traceback
    print("".join(traceback.format_exception(*exc_info)), file=sys.stderr)

def run():
    sys.excepthook = errorHandler
    from scope import scope

    open_file = None
    if len(sys.argv) > 1:
        open_file = sys.argv[1]
    scope.runui(open_file=open_file)

if __name__ == '__main__':
    run()