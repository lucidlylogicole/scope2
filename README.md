# <img src="style/img/scope.png" height="48px;"> Scope IDE

## This version of Scope is no longer suppported. The new version is available [here](https://gitlab.com/lucidlylogicole/scope)

Scope is a lightweight, cross-platform IDE for Python, web development and more. Scope is primarily coded in Python with PyQt for the main UI with some html5 interfaces via Qt's WebEngine browser (Chromium).


## Screenshots
<a href="docs/img/scope_home.png" target="_blank" title="Scope Home Screen"><img src="docs/img/scope_home.png" width=400></a>
<a href="docs/img/scope_editor.png" target="_blank" title="Scope Screenshot"><img src="docs/img/scope_editor.png" width=400></a>
<a href="docs/img/scope_tabs.png" target="_blank" title="Scope File Tabs"><img src="docs/img/scope_tabs.png" width=400></a>
<a href="docs/img/scope_preview.png" target="_blank" title="Scope File Tabs"><img src="docs/img/scope_preview.png" width=400></a>



## Features
- A lightweight IDE with a clean interface that maximizes screen usage
    - looks good on small and large displays
    - keyboard shortcuts to show/hide the plugins (left, bottom, right)
- An innovative tab window
    - multiple rows of file tabs 
    - multiple workspaces can be open at once
- Support for multiple languages (Python, Javascript, HTML, Markdown and more...)
- Multiple options for code editors (Ace, Scintilla)
- Standard plugins
    - Code Outline - for organizing/transversing code
    - Run/Compile code within Scope with an output screen
    - Filebrowser - view, open, create files
    - Splitview preview for html and markdown
    - Search in files plugin
    - Find Files plugin searches through files
    - Install and enable plugins through the Settings interface
- Extend with your own plugins created with Python and PyQt
- Works on Linux, Windows, and Mac

### Running with Python (source)
1. [Download](https://gitlab.com/lucidlylogicole/scope2/-/archive/master/scope2-master.zip) or clone scope.
1. Install [Python 3](https://www.python.org/downloads/) if not installed
    - should work with versions 3.5 and up
2. Install [PyQt5](http://www.riverbankcomputing.com/software/pyqt/download), Qscintilla and QtWebEngine
    - need a version with QtWebEngine and not QtWebKit  
        **python3 -m pip install PyQt5 QScintilla PyQtWebEngine**
5. Run scope.py 
    - ```python3 Scope.py```
        - or use whatever your Python 3 command is

### Linux Shell
Create a shell script to launch Scope
        
        cd $(dirname $(readlink -f $0))
        python3 Scope.py "$@" 

## License
- **[License](LICENSE)** - GNU General Public License (GPL 3) - required by PyQt

## Reference
Thanks to the following tools that Scope is built on:

- [Python](http://python.org) 
- [PyQt](http://www.riverbankcomputing.com/software/pyqt) - UI
- [Scintilla](http://www.scintilla.org/)/QsciScintilla (via [PyQt](http://www.riverbankcomputing.com/software/pyqt))
- [Ace Editor](http://ace.c9.io/) - HTML5 based code editor
- [CommonMark.py](https://github.com/rolandshoemaker/CommonMark-py) - utilized for [markdown](http://commonmark.org/)
- [pyflakes](https://launchpad.net/pyflakes) - for syntax checking for Python

