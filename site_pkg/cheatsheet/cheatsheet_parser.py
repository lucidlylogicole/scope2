import os,sys, collections
import codecs, traceback

import markdown_parser

def parse(filename):
    
    document = []
    title = ''
    cur_header = -1  # Current Header index
    cur_command = -1 # Current Command index 
    cur_option = -1 # Current Option
    cur_object = None  # Current Object index
    
    line_count = 0
    
    with open(filename,'r') as file:
        for line in file:
            line_count += 1
            linestrip = line.lstrip()
            if line.startswith('title>'):
                #---Title
               title = line[6:].strip()
            if line.startswith('h>'):
                #---Header
                hdr = line[2:].strip()
                cur_header += 1
                document.append({'title':hdr,'content':[]})
                cur_command = -1
                cur_option = -1
                cur_object = None
            elif line.startswith('c>'):
                #---Command
                
                # Check for header
                if cur_header == -1:
                    raise ValueError("Command (c>) specified before header (h>): line %s" %line_count)
                
                cmd = line[2:].strip()
                cur_command += 1
                cur_option = -1
                document[cur_header]['content'].append({'cmd':cmd,'description':'','dm':0,'nm':0,'extra':'','notes':''})
                cur_object = None
            
            elif linestrip.startswith('ce>'):
                #---Command Extra content (not bold)
                
                # Check for header
                if cur_header == -1:
                    raise ValueError("Command (c>) specified before command extra content (ce>): line %s" %line_count)
                
                document[cur_header]['content'][cur_command]['extra']= linestrip[3:].strip()
                
            elif linestrip.startswith('d>') or linestrip.startswith('dm>'):
                #---Command Description
                
                # Check for command
                if cur_command == -1:
                    raise ValueError("Command description (d>) specified before command (c>): line %s" %line_count)
                
                document[cur_header]['content'][cur_command]['description'] = linestrip[2:].strip()
                
                # Flag as markdown
                if line.startswith('dm>'):
                    document[cur_header]['content'][cur_command]['dm']=1
                    document[cur_header]['content'][cur_command]['description'] = linestrip[3:].strip()
                cur_object = 'd>'

            elif linestrip.startswith('n>') or linestrip.startswith('nm>'):
                #---Notes
                
                # Check for command
                if cur_command == -1:
                    raise ValueError("Notes (n>) specified before command (c>): line %s" %line_count)
                
                document[cur_header]['content'][cur_command]['notes'] = linestrip[2:].strip()
                
                # Flag as markdown
                if linestrip.startswith('nm>'):
                    document[cur_header]['content'][cur_command]['nm']=1
                    document[cur_header]['content'][cur_command]['notes'] = linestrip[3:].strip()
                cur_object = 'n>'

            elif linestrip.startswith('e>'):
                #---Examples
            
                # Check for command
                if cur_command == -1:
                    raise ValueError("Example (e>) specified before command (c>): line %s" %line_count)
            
                ex = linestrip[2:].strip()
                if 'examples' not in document[cur_header]['content'][cur_command]:
                    document[cur_header]['content'][cur_command]['examples'] = []
                document[cur_header]['content'][cur_command]['examples'].append(ex)
                cur_object = 'e>'
                
            elif linestrip.startswith('o>'):
                #---Options
                
                # Check for command
                if cur_command == -1:
                    raise ValueError("Option (o>) specified before command (c>): line %s" %line_count)
                
                op = linestrip[2:].strip()
                cur_option += 1
                if 'options' not in document[cur_header]['content'][cur_command]:
                    document[cur_header]['content'][cur_command]['options']=[]
                document[cur_header]['content'][cur_command]['options'].append({'option':op,'description':''})
                cur_object = 'o>'
                
            elif linestrip.startswith('od>'):
                #---Option Description
                
                if cur_option == -1:
                    raise ValueError("Option description (od>) specified before option (o>): line %s" %line_count)
                
                opdesc = linestrip[3:].strip()
                document[cur_header]['content'][cur_command]['options'][cur_option]['description'] = opdesc
                cur_object = 'od>'

            elif cur_object != None:
                # Command has not closed
                if cur_object == 'd>':
                    # Append to description
                    document[cur_header]['content'][cur_command]['description'] += '\n'+line.rstrip()
                elif cur_object == 'n>':
                    # Append to description
                    document[cur_header]['content'][cur_command]['notes'] += '\n'+line.rstrip()
                elif cur_object == 'e>':
                    # Append to examples
                    document[cur_header]['content'][cur_command]['examples'][-1] += '\n'+line.rstrip()
                elif cur_object == 'od>':
                    # Append to examples
                    document[cur_header]['content'][cur_command]['options'][cur_option]['description'] += '\n'+line.rstrip()
    
    # Cleanup Whitespace and parse markdown
    for sections in document:
        for cmd in sections['content']:
            if 'description' in cmd:
                cmd['description'] = cmd['description'].rstrip()
                
                # Parse as markdown
                if cmd['dm']:
                    cmd['description'] = markdown_parser.parse(text=cmd['description'])
            
            if 'notes' in cmd:
                cmd['notes'] = cmd['notes'].rstrip()
                if cmd['nm']:
                    cmd['notes'] = markdown_parser.parse(text=cmd['notes'])
                    
            if 'examples' in cmd:
                for e in range(len(cmd['examples'])):
                    cmd['examples'][e] = cmd['examples'][e].rstrip()
    
    return {'title':title,'document':document}

def toHtml(document,**kargs):
    
    template_path = os.path.join(os.path.dirname(__file__),'cheatsheet.thtml')
    if 'template' in kargs and kargs['template'] != None:
        template_path = kargs['template']
    
    import pystache
    docD = {'sections':[]}
    
    if 'title' in document:
        docD['title'] = document['title']
    
    for section in document['document']:
        secD = {'title':section['title'],'commands':[]}
        for cmd in section['content']:
            cmdD = {'cmd':cmd['cmd']}
            cmdD['desc'] = cmd['description']
            cmdD['extra'] = cmd['extra']
            cmdD['notes'] = cmd['notes']
            if 'description' in cmd:
                if not cmd['dm']:
                    cmdD['desc'] = cmd['description'].replace('\n','<br>').replace('  ',' &nbsp;')
            if 'notes' in cmd:
                if not cmd['nm']:
                    cmdD['notes'] = cmd['notes'].replace('\n','<br>').replace('  ',' &nbsp;')
            secD['commands'].append(cmdD)
            if 'examples' in cmd:
                cmdD['examples'] = cmd['examples']
            if 'options' in cmd:
                cmdD['options']= cmd['options']
        docD['sections'].append(secD)
    
    txt = codecs.open(template_path,'r','utf-8').read()
    u_renderer = pystache.Renderer(string_encoding='utf8')
    html = u_renderer.render(txt,docD)
    return str(html)

#---Main
if __name__=='__main__':
    # Command Line Mode - Parse File
    if len(sys.argv) > 1:
        flnm = sys.argv[1]
        
        mode='text'
        if 'html' in sys.argv:
            mode = 'html'
        
        result = ''
        
        try:
            if mode == 'text':
                result = parse(flnm)
            elif mode == 'html':
                result = toHtml(parse(flnm))
        except:
            result = traceback.format_exc()
        
        # Compile
        if 'compile' in sys.argv:
            new_file = sys.argv[-1]
            with codecs.open(new_file,'w','utf-8') as file:
                file.write(result)
        else:
            print(result)