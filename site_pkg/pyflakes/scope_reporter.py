
from . import api

class myReporter:
    def __init__(self,ide,mark_lines,goto=False):
        self.ide = ide
        
        self.error_line = 0
        self.mark_lines = mark_lines
        self.goto = goto
        
    def unexpectedError(self, filename, msg):
##        print('flake.unexpectedError',msg)
        pass
        
    def syntaxError(self, filename, msg, lineno, offset, text):
        if self.mark_lines:
            self.ide.currentEditor().addLineError(lineno-1)
        
        if self.goto:
            self.ide.currentEditor().gotoLine(lineno-1)
        self.error_line = lineno
        
    def flake(self, message):
##        print('flake.message',msg)
        pass


def check_file(ide,txt,filename=''):
    mark_lines = 0
    if hasattr(ide.currentEditor(),'clearLineErrors'):
        ide.currentEditor().clearLineErrors()
        mark_lines = 1
        
    rpt = myReporter(ide,mark_lines)
    errors = api.check(txt,filename,rpt)
    return rpt.error_line