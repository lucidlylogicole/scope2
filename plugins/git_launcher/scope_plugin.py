import os, subprocess
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class Plugin(object):
    title = 'Git'
    location = 'app' # left, bottom, right, app
    widget = None  # The widget for the plugin (set at loadWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        '''Called when loading the plugin'''
        def launch_git_gui():
            wksp_pth = None
##            print(self.parent.currentWorkspace)
            if self.parent.currentWorkspace != None:
                wksp_pth = self.parent.workspaces[self.parent.currentWorkspace]['basefolder']
            else:
                try:
                    wksp_pth = self.parent.settings['plugins']['filebrowser']['defaultPath']
                except:
                    pass
            
            if wksp_pth == None: wksp_pth=''
            if wksp_pth == '':
                if self.parent.currentEditor() != None and self.parent.currentEditor().filename != None:
                    wksp_pth = os.path.dirname(self.parent.currentEditor().filename)
                    
            git_path = "git gui"

            if "git_launcher" in self.parent.settings['plugins']:
                if self.parent.settings['plugins']['git_launcher']['path'] != "":
                    git_path = self.parent.settings['plugins']['git_launcher']['path']
            
            cmd= 'cd "'+wksp_pth+'" && '+git_path
##            print(cmd)
            subprocess.Popen(cmd, shell=True)
            
        # Add button 
        btn = self.parent.addLeftBarButton(qtg.QIcon('icon.png'),tooltip=self.title)
        btn.clicked.connect(launch_git_gui)

        # Add shortcut
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_G,self.parent,btn.click) 