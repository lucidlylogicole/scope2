from . import webview
import os
from PyQt5 import  QtCore as qtc

def addEditor(parent,lang,filename):
    baseurl=None
    if filename != None:
        if os.name =='nt':
            pfx="file:///"
        else:
            pfx="file://"
        burl = qtc.QUrl(pfx+os.path.abspath(os.path.dirname(filename)).replace('\\','/')+'/')
    editor = webview.WebView(parent,baseurl)
    return editor