import os, subprocess
from PyQt5 import QtGui as qtg, QtWidgets as qtw
    
class Plugin(object):
    title = 'Launch Console'
    location = 'app'
    widget = None  # The widget for the plugin (set at getWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        def launch_console():
            self.parent.launchInConsole(cmd='')

        # Add button 
        btn = self.parent.addLeftBarButton(qtg.QIcon('icon.png'),tooltip=self.title)
        btn.clicked.connect(launch_console)
