from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
from PyQt5 import QtWebEngineWidgets as qte
from .preview_ui import Ui_Form
from plugins.webview import webview
import re, os, importlib, subprocess, time

class Preview(qtw.QWidget):
    def __init__(self,parent=None):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ide = parent
        
        self.wdgD={}
        self.prevD={}
        
    def addPreview(self,wdg):
        pwdg = qtw.QWidget(parent=self)
        
        pwdg.webview = webview.WebView(parent=self.ide)
        pwdg.webview.page().parent = self
        layout=qtw.QGridLayout(pwdg)
        layout.setSpacing(0)
        layout.setContentsMargins(0,0,0,0)
        pwdg.setLayout(layout)
        splitter=qtw.QSplitter(qtc.Qt.Vertical,self)

        pwdg.layout().addWidget(splitter)
        splitter.addWidget(pwdg.webview)

##        pwdg.webview.page().setLinkDelegationPolicy(QtWebKit.QWebPage.DelegateAllLinks)
##        pwdg.webview.linkClicked.connect(self.urlClicked)
        pwdg.webview.lastScrollValue=0
        pwdg.webview.loadFinished.connect(self.load_finished)
        
        pwdg.webview.settings().setAttribute(qte.QWebEngineSettings.PluginsEnabled,True)
        pwdg.webview.settings().setAttribute(qte.QWebEngineSettings.JavascriptEnabled,True)
        pwdg.webview.settings().setAttribute(qte.QWebEngineSettings.JavascriptCanOpenWindows,True)
        
##        pwdg.webview.settings().setAttribute(QtWebKit.QWebSettings.PluginsEnabled,True)
##        pwdg.webview.settings().setAttribute(QtWebKit.QWebSettings.JavaEnabled,True)
##        pwdg.webview.settings().setAttribute(QtWebKit.QWebSettings.JavascriptEnabled,True)
##        pwdg.webview.settings().setAttribute(QtWebKit.QWebSettings.JavascriptCanOpenWindows,True)
##        pwdg.webview.settings().setAttribute(QtWebKit.QWebSettings.LocalStorageEnabled,True)
##        pwdg.webview.settings().setLocalStoragePath(self.ide.settingPath)
##        pwdg.webview.settings().enablePersistentStorage(self.ide.settingPath)
##        pwdg.webview.settings().enablePersistentStorage(qtc.QDir.homePath())
        
        # Inspector
        #---todo: add inspector back in
##        pwdg.webview.setupInspector()
##        splitter.addWidget(pwdg.webview.webInspector)
        
        sw_ind = self.ui.sw_prev.count()
        self.ui.sw_prev.insertWidget(sw_ind,pwdg)
        self.ui.sw_prev.setCurrentIndex(sw_ind)

        self.wdgD[wdg] = pwdg
        self.prevD[pwdg]=wdg
    
    def editorTabChanged(self,wdg):
        if wdg in self.wdgD:
            pwdg = self.wdgD[wdg]
            self.ui.sw_prev.setCurrentWidget(pwdg)
        else:
            self.ui.sw_prev.setCurrentIndex(0)
    
    def updatePreview(self,wdg):
        if wdg in self.wdgD:
            if self.wdgD[wdg].isVisible():
                self.previewRun(wdg)
    
    def previewRun(self,wdg,justset=0):
        if wdg not in self.wdgD:
            self.addPreview(wdg)
        else:
            pwdg = self.wdgD[wdg]

        pwdg = self.wdgD[wdg]

        burl = wdg.filename
        if wdg.filename == None: burl=''
        if burl != None:
            if os.name =='nt':
                pfx="file:///"
            else:
                pfx="file://"
            burl = qtc.QUrl(pfx+os.path.abspath(burl).replace('\\','/'))

        html = wdg.getText()

        cmd=None
        if 'run' in self.ide.settings['prog_lang'][wdg.lang]:
            pcmd=self.ide.settings['prog_lang'][wdg.lang]['run'].split('preview ')
            if len(pcmd)>1:
                cmd = pcmd[1]

        if cmd == 'markdown':
            # If markdown generate preview tab
            import site_pkg.commonmark_mod
            html = site_pkg.commonmark_mod.generate(text=html,style='',custom=1)

        elif cmd != None:
            if '{{filename}}' in cmd:
                fcmd = cmd.replace('{{filename}}',wdg.filename)
            else:
                fcmd = cmd+' '+wdg.filename
            if '{{scope}}' in fcmd:
                fcmd = fcmd.replace('{{scope}}',self.ide.scopePath)
            try:
                html = subprocess.check_output(fcmd,shell=True).decode('utf-8')
            except:
                html = '<h2>Error Generating Preview</h2> - Check the settings for this language<br>'
                import traceback
                html += traceback.format_exc().replace('\n','<br>').replace('  ','&nbsp;&nbsp;')
        pwdg.webview.setText(html,burl)
        
    def editorTabClosed(self,wdg):
        if wdg in self.wdgD:
            pwdg = self.wdgD[wdg]
            self.wdgD.pop(wdg)
            self.ui.sw_prev.removeWidget(pwdg)
            pwdg.deleteLater()
            del pwdg
    
    def load_finished(self):
        pwdg = self.ui.sw_prev.currentWidget()
    
    def urlClicked(self,url):
        lnk = str(url.toString())
        pwdg = self.ui.sw_prev.currentWidget()
            # Markdown
        if lnk.startswith('file:') and lnk.endswith('.md'):
            filename = str(url.toLocalFile())
            import site_pkg.commonmark_mod
            html = site_pkg.commonmark_mod.generate(filename,custom=1)
            
            burl = url
            pwdg.webview.setText(html,burl)
            
        elif lnk.startswith('http') or lnk.startswith('www'):
            # External links
            import webbrowser
            webbrowser.open(lnk)
        else:
            pwdg.webview.load_link(url)
        
        return 0