from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
from .outline_ui import Ui_Form
from .outline_tree_ui import Ui_Outline_Tree
import re, os, importlib

class OutlineTree(qtw.QWidget):
    def __init__(self,parent=None):
        qtw.QWidget.__init__(self,parent)
        curdir=os.path.abspath('.')
        os.chdir(os.path.abspath(os.path.dirname(__file__)))
        self.ui = Ui_Outline_Tree()
        self.ui.setupUi(self)
        os.chdir(curdir)

        self.setProperty("class","pluginVertical")
        self.parent = parent
        
        self.ui.l_title.hide()
        self.ui.le_find.textChanged.connect(self.find)

        self.ui.tr_outline.keyPressEvent = self.trkeyPressEvent

    def find(self):
        trwdg = self.ui.tr_outline
        txt = str(self.ui.le_find.text()).lower()
        for t in range(trwdg.topLevelItemCount()):
            itm = trwdg.topLevelItem(t)
            if txt =='' or txt in str(itm.text(0)).lower():
                itm.setHidden(0)
            else:
                itm.setHidden(1)

    def trkeyPressEvent(self,event):
        ky = event.key()
        handled = 0
##        print ky
        if ky in [qtc.Qt.Key_Enter,qtc.Qt.Key_Return]:
            if self.ui.tr_outline.currentItem() != None:
                self.parent.goto(self.ui.tr_outline.currentItem(),0)
                handled = 1

        if not handled:
            qtw.QTreeWidget.keyPressEvent(self.ui.tr_outline,event)
            

class Outline(qtw.QWidget):
    def __init__(self,parent=None):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ide = parent
        self.wdgD = {}
        
        self.show_decorators = 0
        
        # Create blank page for default
        self.ui.sw_outline.insertWidget(0,qtw.QWidget())
        
        self.outlineLangD = {}
        for lang in os.listdir(os.path.join(os.path.dirname(__file__),'lang')):	
            l = lang.split('.')[0]
            if not l.startswith('_'):
                mod = importlib.import_module('plugins.outline.lang.'+l)
                funcs = dir(mod)

                if 'analyzeLine' in funcs:
                    self.outlineLangD[l]=mod.analyzeLine

        self.alwaysUpdate = self.ide.settings['plugins']['outline']['alwaysUpdate']
        if not self.alwaysUpdate:
            self.ide.Events.editorSaved.connect(self.updateOutline)
            
        # Update location
        if 1:
            self.ide.Events.editorVisibleLinesChanged.connect(self.updateLocation)
        
    def analyzeLine(self,wdg,typ):
        return None,None
        
    def addOutline(self,wdg):
        owdg = OutlineTree(parent=self)
        owdg.ui.l_title.setText(wdg.title)
        trwdg = owdg.ui.tr_outline
        sw_ind = self.ui.sw_outline.count()
        self.ui.sw_outline.insertWidget(sw_ind,owdg)
        self.ui.sw_outline.setCurrentIndex(sw_ind)

        self.wdgD[wdg.id] = owdg

        if self.alwaysUpdate==1:
            # Add Text Changed Signal
            if 'editorTextChanged' in dir(wdg):
                wdg.Events.editorChanged.connect(self.updateOutline)
        
        if 'gotoLine' in dir(wdg):
            trwdg.itemDoubleClicked.connect(self.goto)
        
        trwdg.contextMenuEvent = self.outlineMenu
        
        # Updating not needed on start - will be triggered after file loaded
##        self.updateOutline(toggle_view=0)

    def togglePlugin(self):
        self.updateOutline(toggle_view=1,set_focus=1)
        
    def updateOutlineToggle(self):
        self.updateOutline(toggle_view=1)

    def updateOutline(self,wdg=None,toggle_view=0,set_focus=0,txt=None):
        # Get current widget from ide
        if wdg == None:
            wdg = self.ide.currentEditor()
        
        if toggle_view:
            self.ide.showLeftPlugin('outline')
##            self.ide.pluginD['outline'].btn.setChecked(1)
##            if self.ide.ui.fr_left.isHidden():
####                self.ide.ui.fr_left.setVisible(1)
##                self.ide.toggleLeftPlugin(show=1,check_button=0)
##            i=self.ide.ui.tab_left.indexOf(self.ide.pluginD['outline'].widget)
##            self.ide.ui.tab_left.setCurrentIndex(i)
        
        if wdg != None and wdg.id in self.wdgD:
            trwdg = self.wdgD[wdg.id].ui.tr_outline
            if wdg.lang != 'Text' and wdg.lang in self.outlineLangD and 'getText' in dir(wdg):
                
                current_txt = None
                current_itm = None
                if trwdg.currentItem() != None:
                    current_txt = str(trwdg.currentItem().text(0))
                
                
                trwdg.clear()
                
                if txt == None:
                    txt = wdg.getText()
                if txt == None: return
                txtlines = txt.splitlines()
                
                txt_outline = self.outlineLangD[wdg.lang](txtlines)
                
                for t in txt_outline:
                    itmText = t[0]
                    typ = t[1]
                    if typ == 'heading' and itmText.lstrip().lower().startswith('todo'):
                        # TODO Type
                        typ = 'todo'
                        itmText = itmText.lstrip()[4:]
                        if itmText.startswith(':'): itmText= itmText[1:].lstrip()
                    lcnt = t[2]
                    if self.show_decorators or typ != 'decorator':
                        itm =qtw.QTreeWidgetItem([itmText,str(lcnt)])
                        if itmText == current_txt: current_itm = itm
                        trwdg.addTopLevelItem(itm)
                        self.format(itm,typ)
                
                # Update Location if possible
                if 'getVisibleLines' in dir(wdg):
                    lines = wdg.getVisibleLines()
                    self.updateLocation(wdg,lines)
                
                if current_itm != None:
                    trwdg.setCurrentItem(current_itm)
                
                if set_focus:
                    trwdg.setFocus()
    
    def updateLocation(self,wdg,lines=None):
        if self.ide.settings['visibleLineTracking']:
            if wdg.id in self.wdgD:
                trwdg = self.wdgD[wdg.id].ui.tr_outline
                hi=0

                if lines == None:
                    lines = wdg.getVisibleLines()
                
                if lines != None:
                
                    if self.ide.theme == 'light':
                        brsh=qtg.QColor(qtg.QColor(180,180,180,150)) # gray
                    else:
                        brsh=qtg.QColor(qtg.QColor(30,30,30,150)) # gray
                    
                    for t in range(trwdg.topLevelItemCount()-1,-1,-1):
                        itm = trwdg.topLevelItem(t)
                        line = int(str(itm.text(1)))
                        if line>=lines[0] and line<=lines[1]:
                            itm.setBackground(0,brsh)
                            trwdg.scrollToItem(itm,3)
                            hi=1
                        elif line<=lines[0] and not hi:
                            itm.setBackground(0,brsh)
                            trwdg.scrollToItem(itm,3)
                            hi=1
                        else:
                            itm.setBackground(0,qtg.QColor(0,0,0,0))
            
    def outlineMenu(self,event):
        menu = qtw.QMenu('file menu')
        trwdg = self.ui.sw_outline.currentWidget()
        menu.addAction(qtg.QIcon(self.ide.iconPath+'refresh.png'),'Update (Alt+O)')
        
        # Decorator Menu
        menu.addSeparator()
        decAct=menu.addAction(qtg.QIcon(),'Show Decorators')
        decAct.setCheckable(1)
        decAct.setChecked(self.show_decorators)
        menu.addAction(decAct)
        act = menu.exec_(trwdg.ui.tr_outline.cursor().pos())
        if act != None:
            acttxt = str(act.text())
            if acttxt=='Update (Alt+O)':
                self.updateOutline()
            elif acttxt =='Show Decorators':
                self.show_decorators = decAct.isChecked()
                self.updateOutline()
    
    def findFocus(self):
        trwdg = self.ui.sw_outline.currentWidget()
        trwdg.ui.fr_find.show()
        trwdg.ui.le_find.setFocus()
    
    def editorTabChanged(self,wdg):
        if wdg.id in self.wdgD:
            owdg = self.wdgD[wdg.id]
            self.ui.sw_outline.setCurrentWidget(owdg)
        else:
            self.ui.sw_outline.setCurrentIndex(0)
        
    def editorTabClosed(self,wdg):
        if wdg.id in self.wdgD:
            owdg = self.wdgD[wdg.id]
            self.wdgD.pop(wdg.id)
            self.ui.sw_outline.removeWidget(owdg)
            del owdg
        
    def goto(self,itm,col):
        line = int(str(itm.text(1)))
        wdg = self.ide.currentEditor()
        wdg.gotoLine(line)
        self.updateLocation(wdg)
    
    def format(self,itm,typ):
        # Format the tree widget item
        if typ == 'object':
            fnt=qtg.QFont()
            fnt.setBold(1)
            itm.setFont(0,fnt)
            if self.ide.theme == 'light':
                itm.setForeground(0,qtg.QColor(qtg.QColor(42,90,139)))
            else:
                itm.setForeground(0,qtg.QColor(qtg.QColor(52,111,171)))
        elif typ == 'function':
            if self.ide.theme == 'light':
                itm.setForeground(0,qtg.QColor(qtg.QColor(60,131,197)))
            else:
                itm.setForeground(0,qtg.QColor(qtg.QColor(101,191,246)))
        elif typ == 'heading':
            fnt=qtg.QFont()
            fnt.setBold(1)
            itm.setFont(0,fnt)
            if self.ide.theme == 'light':
                itm.setForeground(0,qtg.QColor(qtg.QColor(111,111,111)))
            else:
                itm.setForeground(0,qtg.QColor(qtg.QColor(180,180,180)))
        elif typ=='filename':
            fnt=qtg.QFont()
            fnt.setBold(1)
            itm.setFont(0,fnt)
            itm.setForeground(0,qtg.QColor(qtg.QColor(250,250,250)))
            itm.setBackground(0,qtg.QColor(qtg.QColor(80,80,80)))
        elif typ=='decorator':
            fnt=qtg.QFont()
            itm.setFont(0,fnt)
            if self.ide.theme == 'light':
                itm.setForeground(0,qtg.QColor(qtg.QColor(78,150,78)))
            else:
                itm.setForeground(0,qtg.QColor(qtg.QColor(161,225,150)))
        elif typ=='todo':
            fnt=qtg.QFont()
            fnt.setBold(1)
            itm.setFont(0,fnt)
            itm.setIcon(0,qtg.QIcon(self.ide.iconPath+'bullet_red.png'))
            if self.ide.theme == 'light':
                itm.setForeground(0,qtg.QColor(qtg.QColor(150,0,0)))
            else:
                itm.setForeground(0,qtg.QColor(qtg.QColor(225,156,142)))
##        else:
##            itm.setBackground(0,qtg.QColor(qtg.QColor(161,225,150,0)))