from . import home
from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw

class Plugin(object):
    location = None
    title = 'Home'
    widget = None  # The widget for the plugin (set at getWidget)
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        '''Called when loading the plugin'''
        self.loadWidget()
        
    def loadWidget(self):
        self.widget = home.Home(self.parent)
        self.parent.HomeWidget = self.widget
        icon = qtg.QIcon(self.parent.iconPath+'home.png')
        self.parent.addMainWidget(self.widget.webview,'Home',icon=icon,typ='app')
        
        # Add shortcut
        qtw.QShortcut(qtc.Qt.Key_F8,self.parent,self.widget.toggleHome) 
        qtw.QShortcut(qtc.Qt.ALT+qtc.Qt.Key_H,self.parent,self.widget.toggleHome) 
##        self.btn = self.parent.addLeftBarButton(icon,tooltip=self.title)
        
        # Add Button
        self.btn = qtw.QPushButton()
        self.btn.setIcon(icon)
        self.btn.setIconSize(qtc.QSize(32,32))
        self.btn.setToolTip(self.title)
        layout = self.parent.ui.fr_left_top_btn.layout()
        layout.insertWidget(layout.count()-1,self.btn)
        self.btn.clicked.connect(self.widget.toggleHome)

        return self.widget