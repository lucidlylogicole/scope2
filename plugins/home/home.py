from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw, QtWebEngineWidgets as qte
from PyQt5.QtWebChannel import QWebChannel
import os

class CallHandler(qtc.QObject):
    @qtc.pyqtSlot(str)
    def route(self,text=''):
        self.home.homeClicked(qtc.QUrl(text))


class Home(object):
    def __init__(self,parent):
        self.IDE=parent
    
        # Create home widget
        from plugins.webview import webview
        self.webview=webview.WebView(self.IDE)
    
        self.webview.urlClicked = self.homeClicked

        self.webview.filename = None
        self.webview.contextMenuEvent = self.contextMenuEvent

        # Set up Web Channel for JavaScript
        self.webview.channel = QWebChannel()
        self.webview.handler = CallHandler()
        self.webview.handler.home = self
        self.webview.channel.registerObject('pyhandler', self.webview.handler)
        self.webview.page().setWebChannel(self.webview.channel)

        self.viewHome()
    
    def toggleHome(self,visible=None):
        self.IDE.changeTab(self.webview.id)
        if not self.webview.url().toString().endswith('home'):
            self.viewHome()
        
        # Toggle Filebrowser too
        if 'filebrowser' in self.IDE.pluginD:
            self.IDE.pluginD['filebrowser'].toggle()
            
    def viewHome(self):
        cur_itm=0
        if os.name =='nt':
            pfx="file:///"
        else:
            pfx="file://"
        
        f = open(self.IDE.scopePath+'/plugins/home/home.html','r')
        txt=f.read()
        f.close()
        
        # Get Open Files
        file_txt = ''
        
         # Add New File Links
        nfiles = ''
        for lang in sorted(self.IDE.settings['prog_lang']):
            if lang != 'default' and self.IDE.settings['prog_lang'][lang]['fave']:
                icn = None
                if os.path.exists(self.IDE.iconPath+'files/'+lang+'.png'):
                    icn = self.IDE.iconPath+'files/'+lang+'.png'
                # Set default Icon if language not found
                if icn == None:
                    icn = self.IDE.iconPath+'files/source.png'

                nfiles += '<a onclick="route(\'new:'+lang+'\')" title="new '+lang+'"><div class="newfile"><img class="file-icon" src="'+pfx+icn+'""> '+lang+'</div></a>'
        
        # Grab Editors
        neditors=''
        for e in sorted(self.IDE.editorD):
            ld = self.IDE.editorD[e]
            neditors+='<a onclick="route(\'editor:'+e+'\')"><div class="newfile"><img src="'+self.IDE.editorPath+'/'+e+'/'+e+'.png'+'" class="file-icon"> ' +e+' &nbsp; &rsaquo;</div></a>'
        
        # Add Workspaces
        wksp = ''
        icn_wksp = self.IDE.iconPath+'workspace.png'
        if os.path.exists(self.IDE.settingPath+'/workspaces'):
            for w in sorted(os.listdir(self.IDE.settingPath+'/workspaces'),key=lambda x: x.lower()):
                wksp += '<div class="newfile workspace">'
                wksp += '<a class="wksp-icon" onclick="route(\'workspacemenu:'+w+'\')"><img class="file-icon2" src="'+icn_wksp+'"></a>'
                wksp += '<a class="wksp-text" onclick="route(\'workspace:'+w+'\')">'+w+'</a></div>'

        # Generate HTML
        g=self.IDE.geometry()
        contentD={
            'files':file_txt,
            'height':g.height()/2,
            'new_files':nfiles,
            'new_editors':neditors,
            'current_item':cur_itm,
            'version':self.IDE.version,
            'workspaces':wksp,
        }

        home_url = os.path.abspath(os.path.dirname(__file__)).replace('\\','/')
        burl = qtc.QUrl().fromLocalFile(home_url)
        
        # Background Image
        contentD['bkd_img'] = home_url+'/default_background.jpg'
        bkimgtxt = 'document.body.style.backgroundImage = "url(\''+home_url+'/default_background.jpg'+'\')";'
        if 'home' in self.IDE.settings['plugins'] and 'backgroundImage' in self.IDE.settings['plugins']['home']:
            if self.IDE.settings['plugins']['home']['backgroundImage'] != '':
                contentD['bkd_img'] = self.IDE.settings['plugins']['home']['backgroundImage']

        for ky in contentD:
            txt=txt.replace('{{'+ky+'}}',str(contentD[ky]))

        self.webview.page().setHtml(txt,burl)
    
    def homeClicked(self,url):
        lnk = url.toString().split('/')[-1]
        handled = 1
        if lnk.startswith('home:'):
            self.viewHome()
            handled = 1
        elif lnk.startswith('new:'):
            lang = lnk.split('new:')[1]
            self.IDE.addEditorWidget(lang)
        elif lnk.startswith('workspace:'):
            # Show Workspace
            wk = lnk.split('workspace:')[1]
            if wk=='new':
                new_wksp = self.IDE.workspaceNew()
                if not new_wksp in [None,'']:
                    self.viewHome()
                    handled = 1
            else:
                self.IDE.workspaceOpen(wk)
                handled = 1
        elif lnk.startswith('workspacemenu:'):
            # Edit Workspace Menu
            wk = lnk.split('workspacemenu:')[1]
            wmenu = qtw.QMenu()
            wmenu.addAction(qtg.QIcon(self.IDE.iconPath+'workspace_edit.png'),'Rename')
            wmenu.addAction(qtg.QIcon(self.IDE.iconPath+'workspace_delete.png'),'Delete')
            resp = wmenu.exec_(self.webview.cursor().pos())
            
            if resp != None:
                txt = str(resp.text())
                if txt == 'Rename':
                    self.IDE.workspaceRename(wk)
                    self.viewHome()
                elif txt == 'Delete':
                    self.IDE.workspaceDelete(wk)
                    self.viewHome()
                handled = 1
        elif lnk.startswith('editor:'):
            e = lnk.split('editor:')[1]
            ld = self.IDE.editorD[e]
            
            # Show Editor Menu
            lmenu = qtw.QMenu()
            for l in ld:
                if os.path.exists(self.IDE.iconPath+'/files/'+l.lower()+'.png'):
                    icn = qtg.QIcon(self.IDE.iconPath+'/files/'+l.lower()+'.png')
                else:
                    icn = qtg.QIcon(self.IDE.iconPath+'/files/source.png')
                a=lmenu.addAction(icn,l)
                a.setData(e)
            
            resp = lmenu.exec_(self.webview.cursor().pos())
            
            if resp != None:
                self.IDE.addEditorWidget(str(resp.text()),editor=e)
        
        elif lnk=='filebrowser':
            self.IDE.openFile()
            handled = 0
        elif lnk=='settings':
            self.IDE.openSettings()
            handled = 0
        else:
            self.webview.load_help(url)
        
        return handled

    def contextMenuEvent(self,event):
        pass