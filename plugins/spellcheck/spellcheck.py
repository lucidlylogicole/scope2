from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
import sys, os, re
from .spellcheck_ui import Ui_Form

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
##print os.path.abspath(os.path.dirname(__file__))
##os.environ['PYENCHANT_IGNORE_MISSING_LIB']='True'
import enchant

from PyQt5.QtCore import pyqtSignal

class SpellChecker(qtw.QWidget):
    def __init__(self,parent=None):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ide = parent
        
##        self.setWindowModality(1)
        
##        self.setWindowOpacity(0.6)
##        self.setStyleSheet("#Form {background:white;}")
##        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        
        self.ui.b_cancel.clicked.connect(self.cancel)
        self.ui.b_ok.clicked.connect(self.update)
        
        self.ui.b_undo.clicked.connect(self.undo_word)
        self.ui.b_redo.clicked.connect(self.redo_word)
        
        # Setup Text Editor
        self.ui.te_text.mousePressEvent = self.mousePressEvent2
        self.ui.te_text.contextMenuEvent = self.contextMenuEvent2
        
        self.dict = enchant.Dict("en_US")
        self.highlighter = Highlighter(self.ui.te_text.document())
        self.highlighter.setDict(self.dict)
        
        self.currentEditor=None
        
        self.hide()
    
    def toggle(self):
        if self.isVisible():
            self.hide()
        else:
            self.count = 0
            self.ui.b_ok.setEnabled(0)
            x=self.ide.width()*.05
            y=self.ide.ui.split_left.pos().y()
            w=self.ide.width()*.9
            h=self.ide.ui.split_left.height()-50
            self.setGeometry(x,y,w,h)
            
            # Get Text
            self.currentEditor=self.ide.currentEditor()
            
            if 'getSelectedText' in dir(self.ide.currentEditor()):
                txt=self.ide.currentEditor().getSelectedText()
                if txt == '':
                    if 'selectAll' in dir(self.ide.currentEditor()):
                        self.ide.currentEditor().selectAll()
                        txt=self.ide.currentEditor().getSelectedText()
                if txt != '':
                    self.ui.te_text.setTextCursor(qtg.QTextCursor())
                    self.ui.te_text.setPlainText(txt)
    ##                self.currentEditor.setEnabled(0)
                    self.show()
            
    
    def cancel(self):
        self.hide()
        
    def update(self):
        if self.currentEditor != None:
            self.currentEditor.insertText(self.ui.te_text.toPlainText())
            self.cancel()
    
    def mousePressEvent2(self, event):
        if event.button() == qtc.Qt.RightButton:
            # Rewrite the mouse event to a left button event so the cursor is
            # moved to the location of the pointer.
            event = qtg.QMouseEvent(qtc.QEvent.MouseButtonPress, event.pos(),
                qtc.Qt.LeftButton, qtc.Qt.LeftButton, qtc.Qt.NoModifier)
        qtw.QPlainTextEdit.mousePressEvent(self.ui.te_text, event)

    def keyPressEvent(self,event):
        # Override tab to spaces
        pass

    def contextMenuEvent2(self, event):
        # Select the word under the cursor.
        cursor = self.ui.te_text.textCursor()
        cursor.select(qtg.QTextCursor.WordUnderCursor)
        self.ui.te_text.setTextCursor(cursor)

        # Check if the selected word is misspelled and offer spelling
        # suggestions if it is.
        spell_menu = qtw.QMenu('Spelling Suggestions')
        if self.ui.te_text.textCursor().hasSelection():
            text = str(self.ui.te_text.textCursor().selectedText())
            if not self.dict.check(text):
                
                for word in self.dict.suggest(text):
                    action = SpellAction(word, spell_menu)
                    action.correct.connect(self.correctWord)
                    spell_menu.addAction(action)

        spell_menu.exec_(event.globalPos())

    def correctWord(self, word):
        '''
        Replaces the selected text with word.
        '''
        fmt = qtg.QTextCharFormat()
        cursor = self.ui.te_text.textCursor()
        fmt.setBackground(qtg.QColor(87,255,132))
        
        cursor.beginEditBlock()
        
        cursor.removeSelectedText()
        cursor.setCharFormat(fmt)
        cursor.insertText(word)

        cursor.endEditBlock()
        # Update count
        self.count+=1
        self.ui.b_ok.setEnabled(1)
        self.ui.l_count.setText(str(self.count)+' changes')
    
    def undo_word(self):
        self.count -=1
        self.ui.l_count.setText(str(self.count)+' changes')
        if self.count <=0:
            self.ui.b_ok.setEnabled(0)

    def redo_word(self):
        self.count +=1
        self.ui.l_count.setText(str(self.count)+' changes')
        self.ui.b_ok.setEnabled(1)

    def highlighterEnabled(self):
         return self.highlighter.document() is not None

    def setHighlighterEnabled(self, enable):

        if enable != self.highlighterEnabled():
            if enable:
                self.highlighter.setDocument(self.ui.te_text.document())
            else:
                self.highlighter.setDocument(None)

class Highlighter(qtg.QSyntaxHighlighter):

    WORDS = u'(?iu)[\w\']+'

    def __init__(self, *args):
        qtg.QSyntaxHighlighter.__init__(self, *args)

        self.dict = None

    def setDict(self, dict):
        self.dict = dict

    def highlightBlock(self, text):
        if not self.dict:
            return

        text = text

        format = qtg.QTextCharFormat()
        format.setUnderlineColor(qtc.Qt.red)
        format.setUnderlineStyle(qtg.QTextCharFormat.SpellCheckUnderline)

        for word_object in re.finditer(self.WORDS, text):
            if not self.dict.check(word_object.group()):
                self.setFormat(word_object.start(),
                    word_object.end() - word_object.start(), format)


class SpellAction(qtw.QAction):

    '''
    A special QAction that returns the text in a signal.
    '''

    correct = pyqtSignal(str)

    def __init__(self, *args):
        qtw.QAction.__init__(self, *args)

        self.triggered.connect(lambda x: self.correct.emit(
            str(self.text())))