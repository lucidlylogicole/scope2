# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'find_files.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(568, 341)
        self.gridLayout_2 = QtWidgets.QGridLayout(Form)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setSpacing(4)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.frame_2 = QtWidgets.QFrame(Form)
        self.frame_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(2)
        self.gridLayout.setObjectName("gridLayout")
        self.tr_results = QtWidgets.QTreeWidget(self.frame_2)
        self.tr_results.setStyleSheet(" QTreeWidget {\n"
"border:0px;\n"
"outline:none;\n"
"}\n"
"QTreeWidget::branch:has-siblings:!adjoins-item {\n"
"     border-image: url(style/img/none.png) 0;\n"
" }\n"
" QTreeWidget::branch:has-siblings:adjoins-item {\n"
"     border-image: url(style/img/none.png) 0;\n"
" }\n"
"\n"
" QTreeWidget::branch:!has-children:!has-siblings:adjoins-item {\n"
"     border-image: url(style/img/none.png) 0;\n"
" }")
        self.tr_results.setExpandsOnDoubleClick(False)
        self.tr_results.setObjectName("tr_results")
        self.gridLayout.addWidget(self.tr_results, 2, 0, 1, 3)
        self.frame_3 = QtWidgets.QFrame(self.frame_2)
        self.frame_3.setMaximumSize(QtCore.QSize(16777215, 32))
        self.frame_3.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame_3)
        self.gridLayout_4.setContentsMargins(0, 2, 0, 2)
        self.gridLayout_4.setSpacing(0)
        self.gridLayout_4.setObjectName("gridLayout_4")
        spacerItem = QtWidgets.QSpacerItem(6, 10, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem, 0, 4, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(6, 10, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem1, 0, 1, 1, 1)
        self.b_browse = QtWidgets.QPushButton(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.b_browse.sizePolicy().hasHeightForWidth())
        self.b_browse.setSizePolicy(sizePolicy)
        self.b_browse.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../style/img/file_open.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_browse.setIcon(icon)
        self.b_browse.setObjectName("b_browse")
        self.gridLayout_4.addWidget(self.b_browse, 0, 2, 1, 1)
        self.le_search = QtWidgets.QLineEdit(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(2)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.le_search.sizePolicy().hasHeightForWidth())
        self.le_search.setSizePolicy(sizePolicy)
        self.le_search.setText("")
        self.le_search.setObjectName("le_search")
        self.gridLayout_4.addWidget(self.le_search, 0, 7, 1, 1)
        self.le_path = QtWidgets.QLineEdit(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(4)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.le_path.sizePolicy().hasHeightForWidth())
        self.le_path.setSizePolicy(sizePolicy)
        self.le_path.setStyleSheet("QLineEdit {\n"
"border-left:0px;\n"
"}")
        self.le_path.setObjectName("le_path")
        self.gridLayout_4.addWidget(self.le_path, 0, 3, 1, 1)
        self.b_browse_2 = QtWidgets.QPushButton(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.b_browse_2.sizePolicy().hasHeightForWidth())
        self.b_browse_2.setSizePolicy(sizePolicy)
        self.b_browse_2.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../../style/img/wrench.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_browse_2.setIcon(icon1)
        self.b_browse_2.setCheckable(True)
        self.b_browse_2.setObjectName("b_browse_2")
        self.gridLayout_4.addWidget(self.b_browse_2, 0, 0, 1, 1)
        self.b_search = QtWidgets.QPushButton(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.b_search.sizePolicy().hasHeightForWidth())
        self.b_search.setSizePolicy(sizePolicy)
        self.b_search.setMinimumSize(QtCore.QSize(56, 0))
        self.b_search.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("../../style/img/search.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_search.setIcon(icon2)
        self.b_search.setCheckable(True)
        self.b_search.setObjectName("b_search")
        self.gridLayout_4.addWidget(self.b_search, 0, 8, 1, 1)
        self.le_ext = QtWidgets.QLineEdit(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.le_ext.sizePolicy().hasHeightForWidth())
        self.le_ext.setSizePolicy(sizePolicy)
        self.le_ext.setText("")
        self.le_ext.setObjectName("le_ext")
        self.gridLayout_4.addWidget(self.le_ext, 0, 5, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(6, 10, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_4.addItem(spacerItem2, 0, 6, 1, 1)
        self.gridLayout.addWidget(self.frame_3, 0, 0, 1, 3)
        self.frame = QtWidgets.QFrame(self.frame_2)
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setSpacing(2)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.l_cur_file = QtWidgets.QLabel(self.frame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.l_cur_file.sizePolicy().hasHeightForWidth())
        self.l_cur_file.setSizePolicy(sizePolicy)
        self.l_cur_file.setStyleSheet("")
        self.l_cur_file.setText("")
        self.l_cur_file.setObjectName("l_cur_file")
        self.gridLayout_3.addWidget(self.l_cur_file, 0, 0, 1, 1)
        self.b_search_3 = QtWidgets.QPushButton(self.frame)
        self.b_search_3.setMaximumSize(QtCore.QSize(30, 16777215))
        self.b_search_3.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("../../style/img/arrow_out.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_search_3.setIcon(icon3)
        self.b_search_3.setObjectName("b_search_3")
        self.gridLayout_3.addWidget(self.b_search_3, 0, 1, 1, 1)
        self.b_search_2 = QtWidgets.QPushButton(self.frame)
        self.b_search_2.setMaximumSize(QtCore.QSize(30, 16777215))
        self.b_search_2.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("../../style/img/arrow_in.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_search_2.setIcon(icon4)
        self.b_search_2.setObjectName("b_search_2")
        self.gridLayout_3.addWidget(self.b_search_2, 0, 2, 1, 1)
        self.b_copy = QtWidgets.QPushButton(self.frame)
        self.b_copy.setMaximumSize(QtCore.QSize(30, 16777215))
        self.b_copy.setText("")
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("../../style/img/copy.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_copy.setIcon(icon5)
        self.b_copy.setObjectName("b_copy")
        self.gridLayout_3.addWidget(self.b_copy, 0, 4, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_3.addItem(spacerItem3, 0, 3, 1, 1)
        self.gridLayout.addWidget(self.frame, 3, 0, 1, 3)
        self.fr_settings = QtWidgets.QFrame(self.frame_2)
        self.fr_settings.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.fr_settings.setFrameShadow(QtWidgets.QFrame.Raised)
        self.fr_settings.setObjectName("fr_settings")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.fr_settings)
        self.gridLayout_5.setContentsMargins(-1, 2, 2, 2)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.ckbx_relpath = QtWidgets.QCheckBox(self.fr_settings)
        self.ckbx_relpath.setChecked(True)
        self.ckbx_relpath.setObjectName("ckbx_relpath")
        self.gridLayout_5.addWidget(self.ckbx_relpath, 0, 3, 1, 1)
        self.ckbx_case = QtWidgets.QCheckBox(self.fr_settings)
        self.ckbx_case.setObjectName("ckbx_case")
        self.gridLayout_5.addWidget(self.ckbx_case, 0, 1, 1, 1)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_5.addItem(spacerItem4, 0, 4, 1, 1)
        self.ckbx_reg = QtWidgets.QCheckBox(self.fr_settings)
        self.ckbx_reg.setObjectName("ckbx_reg")
        self.gridLayout_5.addWidget(self.ckbx_reg, 0, 2, 1, 1)
        spacerItem5 = QtWidgets.QSpacerItem(50, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_5.addItem(spacerItem5, 0, 0, 1, 1)
        self.frame_4 = QtWidgets.QFrame(self.fr_settings)
        self.frame_4.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout_6.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_6.setSpacing(4)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.le_ignore_files = QtWidgets.QLineEdit(self.frame_4)
        self.le_ignore_files.setObjectName("le_ignore_files")
        self.gridLayout_6.addWidget(self.le_ignore_files, 0, 4, 1, 1)
        self.label = QtWidgets.QLabel(self.frame_4)
        self.label.setObjectName("label")
        self.gridLayout_6.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.frame_4)
        self.label_2.setObjectName("label_2")
        self.gridLayout_6.addWidget(self.label_2, 0, 3, 1, 1)
        self.le_ignore_dirs = QtWidgets.QLineEdit(self.frame_4)
        self.le_ignore_dirs.setObjectName("le_ignore_dirs")
        self.gridLayout_6.addWidget(self.le_ignore_dirs, 0, 1, 1, 1)
        spacerItem6 = QtWidgets.QSpacerItem(10, 10, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_6.addItem(spacerItem6, 0, 2, 1, 1)
        self.gridLayout_5.addWidget(self.frame_4, 1, 1, 1, 4)
        self.gridLayout.addWidget(self.fr_settings, 1, 0, 1, 3)
        self.gridLayout_2.addWidget(self.frame_2, 0, 0, 1, 1)

        self.retranslateUi(Form)
        self.le_search.returnPressed.connect(self.b_search.click)
        self.b_search_3.clicked.connect(self.tr_results.expandAll)
        self.b_search_2.clicked.connect(self.tr_results.collapseAll)
        self.le_ext.returnPressed.connect(self.b_search.click)
        self.le_path.returnPressed.connect(self.b_search.click)
        self.b_browse_2.toggled['bool'].connect(self.fr_settings.setVisible)
        QtCore.QMetaObject.connectSlotsByName(Form)
        Form.setTabOrder(self.le_path, self.le_ext)
        Form.setTabOrder(self.le_ext, self.le_search)
        Form.setTabOrder(self.le_search, self.b_search)
        Form.setTabOrder(self.b_search, self.tr_results)
        Form.setTabOrder(self.tr_results, self.b_search_2)
        Form.setTabOrder(self.b_search_2, self.b_browse)
        Form.setTabOrder(self.b_browse, self.b_search_3)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.tr_results.setSortingEnabled(True)
        self.tr_results.headerItem().setText(0, _translate("Form", "File"))
        self.tr_results.headerItem().setText(1, _translate("Form", "Line(s)"))
        self.tr_results.headerItem().setText(2, _translate("Form", "Path / Code"))
        self.b_browse.setToolTip(_translate("Form", "select a path"))
        self.le_search.setToolTip(_translate("Form", "Search Term"))
        self.le_search.setPlaceholderText(_translate("Form", "search term"))
        self.le_path.setToolTip(_translate("Form", "Path to search (recursively)"))
        self.le_path.setPlaceholderText(_translate("Form", "path"))
        self.b_browse_2.setToolTip(_translate("Form", "search settings"))
        self.b_search.setToolTip(_translate("Form", "search"))
        self.le_ext.setToolTip(_translate("Form", "<html><head/><body><p>Extensions</p><p>   - example: .py</p><p>   - leave blank to search all</p><p>   - multiple are comma separated (.py,.js)</p></body></html>"))
        self.le_ext.setPlaceholderText(_translate("Form", "extensions"))
        self.b_search_3.setToolTip(_translate("Form", "expand all"))
        self.b_search_2.setToolTip(_translate("Form", "collapse all"))
        self.b_copy.setToolTip(_translate("Form", "copy file list to clipboard"))
        self.ckbx_relpath.setToolTip(_translate("Form", "Search term is a regular expression (Python format)"))
        self.ckbx_relpath.setText(_translate("Form", "Relative Paths"))
        self.ckbx_case.setText(_translate("Form", "Match case"))
        self.ckbx_reg.setToolTip(_translate("Form", "Search term is a regular expression (Python format)"))
        self.ckbx_reg.setText(_translate("Form", "Regular Expression"))
        self.le_ignore_files.setToolTip(_translate("Form", "Files to ignore (separate by comma)"))
        self.le_ignore_files.setText(_translate("Form", ".gitignore"))
        self.label.setText(_translate("Form", "Ignore Dirs:"))
        self.label_2.setText(_translate("Form", "Ignore Files:"))
        self.le_ignore_dirs.setToolTip(_translate("Form", "Directories to ignore (separate by comma)"))
        self.le_ignore_dirs.setText(_translate("Form", ".git"))

