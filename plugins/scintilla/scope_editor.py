from . import scintilla
from PyQt5 import Qsci

##class Settings(object):
##    '''Modifiable settings and their defaults'''

class Editor(object):
    title = 'Plugin Title'
    location = 'app' # left, bottom, right, app
##    settings = Settings.__dict__
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        '''Called when loading the plugin'''
        
    def getWidget(self, lang, **kargs):
        lex = None
        
        if lang in self.langD:
            try:
                ldic = locals()
                exec('lex = '+self.langD[lang]+'()',globals(),ldic)
                lex = ldic['lex']
            except:
                print('lexer (%s) failed to load' %lang )

        editor = scintilla.Sci(self.parent,lex,lang)
        self.parent.Events.themeChanged.connect(editor.setupStyle)
        
        return editor

    def getLang(self):
        # Setup languages
        self.langD = {
            'ui':'Qsci.QsciLexerXML',
            'qml':'Qsci.QsciLexerJavaScript',
            'json':'Qsci.QsciLexerJavaScript',
            
        }

        for l in sorted(dir(Qsci)):
            if l.startswith('QsciLexer'):
                lang = l[9:].lower()
                if l != '':
                    self.langD[lang] = 'Qsci.'+l
        
        self.langD['python'] = 'scintilla.PyLexer'
        
        return sorted(self.langD)