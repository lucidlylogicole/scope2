# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'console.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(580, 272)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../scope/plugins/py_console/icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Form.setWindowIcon(icon)
        self.gridLayout_2 = QtWidgets.QGridLayout(Form)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.tb_view = QtWidgets.QTextBrowser(Form)
        self.tb_view.setStyleSheet("QTextBrowser {\n"
"    background:rgb(30,30,30);\n"
"    color:white;\n"
"    border:0px;\n"
"}")
        self.tb_view.setObjectName("tb_view")
        self.gridLayout_2.addWidget(self.tb_view, 0, 0, 1, 1)
        self.frame = QtWidgets.QFrame(Form)
        self.frame.setStyleSheet("QFrame#frame {\n"
"   background-color:rgb(50,50,50);\n"
"   color:white;\n"
"   border:0px;\n"
"}\n"
"QLineEdit {\n"
"border:0px;\n"
"color:white;\n"
"background:transparent;\n"
"}\n"
"QLabel {\n"
"    color:rgb(38,90,150);\n"
"font-weight:bold;\n"
"}")
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout = QtWidgets.QGridLayout(self.frame)
        self.gridLayout.setContentsMargins(2, 4, 2, 1)
        self.gridLayout.setSpacing(2)
        self.gridLayout.setObjectName("gridLayout")
        self.l_prompt = QtWidgets.QLabel(self.frame)
        self.l_prompt.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.l_prompt.setObjectName("l_prompt")
        self.gridLayout.addWidget(self.l_prompt, 0, 0, 1, 1)
        self.gridLayout_2.addWidget(self.frame, 2, 0, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Python Console"))
        self.l_prompt.setText(_translate("Form", ">>>"))

