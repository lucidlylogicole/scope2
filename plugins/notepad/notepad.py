from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
import time, os, codecs

class Notepad(qtw.QTabWidget):
    def __init__(self,parent):
        self.IDE = parent
        qtw.QTabWidget.__init__(self,parent)

        # Tab Settings
        self.setTabsClosable(1)
        self.setMovable(1)
        self.tabCloseRequested.connect(self.closeTab)
        self.currentChanged.connect(self.changeTab)
        self.mouseDoubleClickEvent = self.dblclick
        self.contextMenuEvent = self.rightclick
        
        # Check Settings folder and create
        self.folder = os.path.join(self.IDE.settingPath,'notepad')
        if 'notepad' in self.IDE.settings['plugins'] and 'folder' in self.IDE.settings['plugins']['notepad']:
            if self.IDE.settings['plugins']['notepad']['folder'] != '':
                self.folder = self.IDE.settings['plugins']['notepad']['folder']
        if not os.path.exists(self.folder):
            os.mkdir(self.folder)

        # Auto Save Timer
        self.fileCheck_ignore = []
        self.fileCheckTimer = qtc.QTimer()
        self.fileCheckTimer.setInterval(2000)
        self.fileCheckTimer.timeout.connect(self.checkSave)
        self.fileCheckTimer.start()
        
        # Check for existing files
        self.loadFolder()
        
        # Add A new pad if none
        if self.count() == 0:
            self.addPad()

    def closeTab(self,ind):
        ok = 0
        if self.widget(ind).PATH != None:
            ok = 1
        elif not os.path.exists(os.path.join(self.folder,self.widget(ind).NAME)):
            ok = 1
        else:
            # Delete File
            resp = qtw.QMessageBox.warning(self,'Delete Note','Do you want to close and <b>delete</b> this note?',qtw.QMessageBox.Yes | qtw.QMessageBox.No)
            if resp == qtw.QMessageBox.Yes:
                self.widget(ind).CHANGED = 0
                os.remove(os.path.join(self.folder,self.widget(ind).NAME))
                ok = 1
        if ok:
            self.widget(ind).CHANGED = 0
        
            # Remove Tab
            self.removeTab(ind)
            
            # Add blank one if nothing
            if self.count() == 0:
                self.addPad()
    
    def dblclick(self,event):
        self.addPad()
##        self.setCurrentIndex(self.count()-1)

    def rightclick(self,event):
        print(event.type())
    
    def changeTab(self,ind):
        self.last_save = time.time()

    def loadFolder(self,fldr=None):
        if fldr != None:
            self.folder = fldr

        for fl in os.listdir(self.folder):
            if os.path.isfile(os.path.join(self.folder,fl)):
                self.addPad(fl)

    def addPad(self,name=None,path=None):
        if name == None:
            for i in range(1,30):
                found = 0
                for t in range(self.count()):
                    if self.widget(t).NAME == 'New '+str(i):
                        found = 1
                if found:
                    pass
##                if i < self.count():
####                if self.widget(i) != None:
##                    if self.widget(i).NAME == 'New '+str(i):
##                        pass
                elif not os.path.exists(os.path.join(self.folder,'New '+str(i))):
                    name = 'New '+str(i)
                    break
        te = NoteEditor(self,name,path=path)
        tab_name = te.NAME
        if te.toPlainText().startswith('# '):
##            print(te.toPlainText().split('\n')[0][2:])
            tab_name = te.toPlainText().split('\n')[0][2:]
        self.addTab(te,tab_name)
        self.setCurrentIndex(self.count()-1)

    def checkSave(self):
        for i in range(self.count()):
            wdg = self.widget(i)
            # Check if changed in different scope
            pth = os.path.join(self.folder,wdg.NAME)
            if os.path.exists(pth) and os.path.getmtime(pth) > wdg.modTime and wdg.modTime != None:
                resp = qtw.QMessageBox.warning(self,'File Modified',str(wdg.NAME)+' has been modified.<br><<br>Do you want to reload it?',qtw.QMessageBox.Yes,qtw.QMessageBox.No)
                if resp == qtw.QMessageBox.Yes:
                    wdg.modTime = os.path.getmtime(pth)
                    with codecs.open(pth,'r','utf-8') as f:
                        txt = f.read()
                    wdg.IGNORE_CHANGES = 1
                    wdg.setPlainText(txt)
                    qtw.QApplication.processEvents()
                    wdg.IGNORE_CHANGES = 0
                wdg.CHANGED = 0
                wdg.modTime = os.path.getmtime(pth)

            # Check if notepad changed
            elif wdg.CHANGED:
                wdg.save()
            if wdg.toPlainText().startswith('# '):
                self.setTabText(i,wdg.toPlainText().split('\n')[0][2:])
##            else:
##                self.setTabText(i,wdg.NAME)
                

class NoteEditor(qtw.QPlainTextEdit):
    def __init__(self,parent,name,path=None):
        self.Notepad = parent
        qtw.QTextEdit.__init__(self,parent)
        self.NAME = name
        self.CHANGED = 0
        self.PATH = path
        self.modTime = None
        self.IGNORE_CHANGES = 0
        self.setTabStopWidth(20)
        
        # Open file
        if path == None:
            pth = os.path.join(self.Notepad.folder,name)
        else:
            pth = os.path.join(path,name)
            self.setReadOnly(1)
        if os.path.exists(pth):
##            with open(pth,'r') as f:
            with codecs.open(pth,'r','utf-8') as f:
                self.setPlainText(f.read())
            self.modTime = os.path.getmtime(pth)
        fnt = qtg.QFont()
        fnt.setFamily('Courier')
        self.setFont(fnt)
        self.textChanged.connect(self.changed)
        
##        qtw.QShortcut(qtc.Qt.CTRL+qtc.Qt.Key_S,self,self.save) 
    
    def changed(self):
        if self.PATH == None and not self.IGNORE_CHANGES:
            self.CHANGED = 1

    def save(self):
        pth = os.path.join(self.Notepad.folder,self.NAME)
##        with open(pth,'w') as f:
        with codecs.open(pth,'w','utf-8') as f:
            f.write(self.toPlainText())
        self.CHANGED = 0
        self.modTime = os.path.getmtime(pth)

    def dropEvent(self,event):
        handled=False
        if event.mimeData().urls():
            for f in event.mimeData().urls():
                pths = os.path.split(str(f.toLocalFile()))
                self.Notepad.addPad(pths[1],path=pths[0])
            handled=True

        if not handled:
            qtw.QTextEdit.dropEvent(self,event)