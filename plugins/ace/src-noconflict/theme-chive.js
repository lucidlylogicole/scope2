ace.define("ace/theme/chive",["require","exports","module","ace/lib/dom"], function(require, exports, module) {

exports.isDark = false;
exports.cssClass = "ace-chive";
exports.cssText = ".ace-chive .ace_gutter {\
background: #ebebeb;\
color: #333;\
overflow : hidden;\
}\
.ace-chive .ace_print-margin {\
width: 0px;\
background: #e8e8e8;\
}\
.ace-chive {\
background-color: rgb(250,250,250);\
color: black;\
}\
.ace-chive .ace_cursor {\
color: black;\
}\
.ace-chive .ace_invisible {\
color: rgb(191, 191, 191);\
}\
.ace-chive .ace_constant.ace_buildin {\
color: rgb(88, 72, 246);\
}\
.ace-chive .ace_constant.ace_language {\
color: rgb(88, 92, 246);\
}\
.ace-chive .ace_constant.ace_library {\
color: rgb(6, 150, 14);\
}\
.ace-chive .ace_invalid {\
background-color: rgb(153, 0, 0);\
color: white;\
}\
.ace-chive .ace_fold {\
}\
.ace-chive .ace_support.ace_function {\
color: rgb(45,141,88);\
}\
.ace-chive .ace_support.ace_constant {\
color: rgb(6, 150, 14);\
}\
.ace-chive .ace_support.ace_type,\
.ace-chive .ace_support.ace_class\
.ace-chive .ace_support.ace_other {\
color: rgb(109, 121, 222);\
}\
.ace-chive .ace_variable.ace_parameter {\
font-style:italic;\
color:#FD971F;\
}\
.ace-chive .ace_keyword.ace_operator {\
color: rgb(104, 118, 135);\
}\
.ace-chive .ace_comment {\
color: #236e24;\
}\
.ace-chive .ace_comment.ace_doc {\
color: #236e24;\
}\
.ace-chive .ace_comment.ace_doc.ace_tag {\
color: #236e24;\
}\
.ace-chive .ace_constant.ace_numeric {\
color: rgb(0, 0, 205);\
}\
.ace-chive .ace_variable {\
color: rgb(49, 132, 149);\
}\
.ace-chive .ace_xml-pe {\
color: rgb(104, 104, 91);\
}\
.ace-chive .ace_entity.ace_name.ace_function {\
color: #073c8a;\
}\
.ace-chive .ace_heading {\
color: rgb(149,19,19);\
}\
.ace-chive .ace_list {\
color:rgb(17,68,105);\
}\
.ace-chive .ace_marker-layer .ace_selection {\
background: rgb(181, 213, 255);\
}\
.ace-chive .ace_marker-layer .ace_step {\
background: rgb(252, 255, 0);\
}\
.ace-chive .ace_marker-layer .ace_stack {\
background: rgb(164, 229, 101);\
}\
.ace-chive .ace_marker-layer .ace_bracket {\
margin: -1px 0 0 -1px;\
border: 1px solid rgb(192, 192, 192);\
}\
.ace-chive .ace_marker-layer .ace_active-line {\
background: rgba(0, 0, 0, 0.07);\
}\
.ace-chive .ace_gutter-active-line {\
background-color : #dcdcdc;\
}\
.ace-chive .ace_marker-layer .ace_selected-word {\
background: rgb(250, 250, 255);\
border: 1px solid rgb(200, 200, 250);\
}\
.ace-chive .ace_storage,\
.ace-chive .ace_keyword,\
.ace-chive .ace_meta.ace_tag {\
color: rgb(32,74,135);\
}\
.ace-chive .ace_string.ace_regex {\
color: rgb(255, 0, 0)\
}\
.ace-chive .ace_string {\
color: #1A1AA6;\
}\
.ace-chive .ace_entity.ace_other.ace_attribute-name {\
color: #994409;\
}\
.ace-chive .ace_indent-guide {\
background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAACCAYAAACZgbYnAAAAE0lEQVQImWP4////f4bLly//BwAmVgd1/w11/gAAAABJRU5ErkJggg==\") right repeat-y;\
}\
";

var dom = require("../lib/dom");
dom.importCssString(exports.cssText, exports.cssClass);
});
