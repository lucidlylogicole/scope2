import os

class Editor(object):
    
    def __init__(self,parent=None):
        self.parent = parent
    
    def load(self):
        '''Called when loading the plugin'''
        
    def getWidget(self, lang, **kargs):
        '''Return the Editor Widget'''
        from . import ace
        editor = ace.Ace(self.parent,lang)
        return editor
        
    def getLang(self):
        '''Get Languages'''
        fld = os.path.abspath(os.path.dirname(__file__)).replace('\\','/')+'/src-noconflict/'
        lexers = []
        for f in sorted(os.listdir(fld)):
            if f.startswith('mode'):
                lexers.append(f[5:-3])
        
        return lexers
