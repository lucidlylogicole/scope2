# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'filebrowser.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(231, 434)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.tr_dir = QtWidgets.QTreeWidget(Form)
        self.tr_dir.setStyleSheet("QTreeWidget {\n"
"border-bottom-left-radius:5px;\n"
"border-bottom-right-radius:5px;\n"
"show-decoration-selected: 0;\n"
"padding-left:4px;\n"
"}\n"
"QTreeWidget::branch {  border-image: url(none.png); }\n"
"\n"
"")
        self.tr_dir.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.tr_dir.setIndentation(12)
        self.tr_dir.setRootIsDecorated(False)
        self.tr_dir.setExpandsOnDoubleClick(False)
        self.tr_dir.setObjectName("tr_dir")
        self.tr_dir.headerItem().setText(0, "1")
        self.tr_dir.header().setVisible(False)
        self.gridLayout.addWidget(self.tr_dir, 1, 0, 1, 2)
        self.frame = QtWidgets.QFrame(Form)
        self.frame.setStyleSheet("QFrame#frame {\n"
"     background:rgb(40,40,40);\n"
"    padding:2px 4px;\n"
"margin:2px 0px;\n"
"border-radius:4px;\n"
"}")
        self.frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.b_browse = QtWidgets.QPushButton(self.frame)
        self.b_browse.setMaximumSize(QtCore.QSize(24, 16777215))
        self.b_browse.setStyleSheet("QPushButton {\n"
"background:rgb(40,40,40);\n"
"border:0;\n"
"outline:0;\n"
"}\n"
"QPushButton:hover {\n"
"background:rgb(50,50,50);\n"
"}")
        self.b_browse.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../style/img/file_open2.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.b_browse.setIcon(icon)
        self.b_browse.setIconSize(QtCore.QSize(20, 20))
        self.b_browse.setCheckable(True)
        self.b_browse.setObjectName("b_browse")
        self.gridLayout_2.addWidget(self.b_browse, 0, 0, 1, 1)
        self.le_root = QtWidgets.QLineEdit(self.frame)
        font = QtGui.QFont()
        font.setPointSize(8)
        self.le_root.setFont(font)
        self.le_root.setStyleSheet("background:transparent;\n"
"border:0px;\n"
"padding:2px;")
        self.le_root.setProperty("class", "")
        self.le_root.setObjectName("le_root")
        self.gridLayout_2.addWidget(self.le_root, 0, 1, 1, 1)
        self.gridLayout.addWidget(self.frame, 0, 0, 1, 2)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "File Browser"))
        self.tr_dir.setProperty("class", _translate("Form", "pluginVertical"))
        self.b_browse.setToolTip(_translate("Form", "Select root folder"))
        self.le_root.setText(_translate("Form", "/"))
        self.le_root.setPlaceholderText(_translate("Form", "Enter Root Path"))

