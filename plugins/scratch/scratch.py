from PyQt5 import QtCore as qtc, QtGui as qtg, QtWidgets as qtw
from .scratch_ui import Ui_Form
import os

ignore_ext = ['pyc']

class Scratch(qtw.QWidget):
    def __init__(self,parent=None):
        qtw.QWidget.__init__(self,parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ide = parent